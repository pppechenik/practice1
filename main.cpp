//#include <iostream>

#ifndef UNICODE
#define UNICODE
#endif

// т.к. работаем с winapi - подключаем заголовок:
#include "windows.h"

// Объявляем константы
const int img_x = 0, img_y = 0; // <- относительная позиция картинки, устанавливаемая относительно правого нижнего угла.
// так как ничего с файла программно считывать не нужно:
const wchar_t *image_path = L"./Фигура.bmp"; // <- Расположения файла с фигурой
const int img_w = 541, img_h = 425; // <- размер картинки, взято из файла.

// Обьявляяем переменные, с которыми будем работать
HBITMAP loadImage; // Загруженная картинка
UINT wWidth, wHeight; // переменные ширина и высота окна

// Обявление функции обратного вызова, чтобы программа о ней знала и смогла найти
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

// 1. Создаем входную точку для Winapi
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   PSTR lpCmdLine, INT nCmdShow) {
    const wchar_t g_szClassName[] = L"myWindowClass";

    // Регистрируем

    WNDCLASSEX wc; // структура https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-wndclassexa
    HWND hwnd;
    MSG Msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 0;
    wc.lpfnWndProc = WindowProc; // <<- Наша CALLBACK функция winapi
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    /*
    Цвет фона в формате RGB:
     RGB(255,0,0) - Красный
     RGB(0,255,0) - Зеленый
     RGB(0,0,255) - Синий

     RGB(150,150,75) - оттенок желтого(?не совсем), отличного от картинки
     */
    wc.hbrBackground = CreateSolidBrush(RGB(150,150,75)); //(HBRUSH) (COLOR_WINDOW + 26);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = g_szClassName;
    wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    // Проверяем регистрацию, завершаем работу если неудачно. -> https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-registerclassexa
    if (!RegisterClassEx(&wc)) {
        MessageBox(NULL, L"Окно не может быть зарегистрировано!", L"Ошибка", MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    // Создание окна. Структура -> https://docs.microsoft.com/ru-ru/windows/win32/api/winuser/nf-winuser-createwindowexa
    hwnd = CreateWindowEx(
            WS_EX_CLIENTEDGE,
            g_szClassName,
            L"Заголовок окна",
            WS_OVERLAPPEDWINDOW,

            // Начальные позиция и размеры окна
            CW_USEDEFAULT, CW_USEDEFAULT, 1024, 768,

            NULL, NULL, hInstance, NULL);

    // Если окно не создано, завершаем работу
    if (hwnd == NULL) {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    // Создаем петлю для обрааботки сообщений
    MSG msg = {};
    while (GetMessage(&msg, NULL, 0, 0) > 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    // Выход из главной функции
    return 0;
}

void drawBmp(HDC hdc, HBITMAP bmpimg, int x, int y) {
    HDC
            hdcTemp,
            hdcBg, hdcFt, hdcMem, hdcObj;

    BITMAP bm;
    POINT ptSize;
    HBITMAP bmBg, bmFt, bmMem, bmObj;

    // создаем контекст устройства в памяти для дальнейших с ним манипуляций
    hdcTemp = CreateCompatibleDC(hdc);
    // выбираем изображение контексту - картинка из параметров
    SelectObject(hdcTemp, bmpimg);

    // извлекаем информацию bpj,hf;tybz, в перемменную 'bm'
    GetObject(bmpimg, sizeof(BITMAP), (LPSTR) &bm);
    ptSize.x = bm.bmWidth;
    ptSize.y = bm.bmHeight;
    // преобразует координаты устройства в логические координаты.
    DPtoLP(hdcTemp, &ptSize, 1);

    // создаем контекст устройства в памяти.
    hdcBg = CreateCompatibleDC(hdc); // <- Background Обьекта
    hdcFt = CreateCompatibleDC(hdc); // <- Front обьектa
    hdcMem = CreateCompatibleDC(hdc); // <- Для обьединения
    hdcObj = CreateCompatibleDC(hdc); // <- Для наложения

    // создает растровое изображение
    bmBg = CreateBitmap(ptSize.x, ptSize.y, 1, 1, NULL);// background, однобитный
    bmFt = CreateBitmap(ptSize.x, ptSize.y, 1, 1, NULL);// objet, однобитный
    bmMem = CreateCompatibleBitmap(hdc, ptSize.x, ptSize.y);
    bmObj = CreateCompatibleBitmap(hdc, ptSize.x, ptSize.y);

    // Прежде чем приложение сможет использовать контекст устройства памяти для операций рисования,
    // оно должно выбрать растровое изображение правильной ширины и высоты в контексте устройства
    SelectObject(hdcBg, bmBg);
    SelectObject(hdcFt, bmFt);
    SelectObject(hdcMem, bmMem);
    SelectObject(hdcObj, bmObj);

    // Копируем фон в рабочий дескриптор
    BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdc, x, y, SRCCOPY);
    // Купируем обьект из исходного
    BitBlt(hdcObj, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);

    // Создание маски обьекта (монохромный) из исходного.
    BitBlt(hdcFt, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);
    // Создание маски фона путем инвертирования цветов обьекта.
    BitBlt(hdcBg, 0, 0, ptSize.x, ptSize.y, hdcFt, 0, 0, NOTSRCCOPY);
    // Накладываем маску обьекта на скопированный фон.
    BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcFt, 0, 0, SRCAND);
    // Накладываем маску фона на обьект.
    BitBlt(hdcObj, 0, 0, ptSize.x, ptSize.y, hdcBg, 0, 0, SRCAND);
    // Объединяем цвета
    BitBlt(hdcMem, 0, 0, ptSize.x, ptSize.y, hdcObj, 0, 0, SRCPAINT);
    // Помещаем на экран.
    BitBlt(hdc, x, y, ptSize.x, ptSize.y, hdcMem, 0, 0, SRCCOPY);
    // Зыкрываем дырки, чтобы маска не сочилась.
    BitBlt(hdcObj, 0, 0, ptSize.x, ptSize.y, hdcTemp, 0, 0, SRCCOPY);

    // Выбираем объекты
    SelectObject(hdcBg, bmBg);
    SelectObject(hdcFt, bmFt);
    SelectObject(hdcMem, bmMem);
    SelectObject(hdcObj, bmObj);

    // Очищаемм память
    DeleteDC(hdcBg);
    DeleteDC(hdcFt);
    DeleteDC(hdcMem);
    DeleteDC(hdcObj);

    DeleteDC(hdcTemp);
}
// Реализуем объявленую выше функцию обрааботки сообщений. Cnhernehf -> https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/ms633573(v=vs.85)
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    // Перечислеие типов сообщений
    switch (uMsg) {
        case WM_SIZE: { // <- Ловим момент изменения размер окна, записываем в переменные
            wWidth = LOWORD(lParam);
            wHeight = HIWORD(lParam);
            // Указываем Winapi что отрисованное окно не валидно, и его нужно перерисовать
            InvalidateRect(hwnd, 0, TRUE);
        }
            break;
        case WM_CREATE: { // <- Момент создания окна, загружаем нашу картинку из файла
            loadImage = (HBITMAP) LoadImage(nullptr, image_path, IMAGE_BITMAP, img_w, img_h, LR_LOADFROMFILE);
        }
            break;
        case WM_PAINT: { // <- Момент отрисовки
            HDC hdc;
            PAINTSTRUCT ps;

            // Тут получаем размеры окна, отнимаем размер изображения и точки позиции. (привязка к ниэнему правому краю)
            // int x = img_x, y = img_y; // <-- Привязка к верхнему левому краю
            // int x = ( ( ( wWidth - img_w ) / 2 ) + img_x), y = ( ( ( wHeight - img_h ) / 2 ) - img_y); // <-- Привязка к центру
            int
                    x = ((wWidth - img_w) + img_x),
                    y = ((wHeight - img_h) - img_y);

            hdc = BeginPaint(hwnd, &ps); // начало рисования

            // Рисуем загруженную картинку
            drawBmp(hdc, loadImage, x, y);

            EndPaint(hwnd, &ps); // конец рисования
        }
            break;
        case WM_CLOSE: { // <- Момент закрытия окна
            DestroyWindow(hwnd);
        }
            break;
        case WM_DESTROY: { // <- Момент уничтожения окна, удаляем объект картинки
            DeleteObject(loadImage);
            PostQuitMessage(0);
        }
            break;
        default: // <- обработка сообщений по-умолчанию. Вызывается, если тип сообщений нами не обрабатывается.
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}
